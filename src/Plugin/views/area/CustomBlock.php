<?php

namespace Drupal\views_area_custom_block\Plugin\views\area;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an area handler which renders an entity in a certain view mode.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_area_custom_block")
 */
class CustomBlock extends AreaPluginBase {

  /**
   * Stores the entity type of the result entities.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a new Entity instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Contains the config target identifier for the entity.
    $options['target'] = ['default' => ''];
    $options['view_mode'] = ['default' => 'default'];
    $options['bypass_access'] = ['default' => FALSE];
    $options['display_title'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['view_mode'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions('block'),
      '#title' => $this->t('View mode'),
      '#default_value' => $this->options['view_mode'],
    ];

    $target = $this->options['target'];
    if ($target_entity = $this->entityRepository->loadEntityByConfigTarget('block_content', $this->options['target'])) {
      $target = $target_entity->id();
    }
    $form['target'] = [
      '#title' => $this->t('Block ID'),
      '#type' => 'textfield',
      '#default_value' => $target,
    ];

    $form['display_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display title'),
      '#default_value' => !empty($this->options['display_title']),
    ];

    $form['bypass_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass access checks'),
      '#description' => $this->t('If enabled, access permissions for rendering the entity are not checked.'),
      '#default_value' => !empty($this->options['bypass_access']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    // Load the referenced entity and store its config target identifier if
    // the target does not contains tokens.
    // @todo Use a method to check for tokens in
    //   https://www.drupal.org/node/2396607.
    $options = $form_state->getValue('options');
    if ($entity = $this->entityTypeManager->getStorage('block_content')->load($options['target'])) {
      $options['target'] = $entity->getConfigTarget();
      $form_state->setValue('options', $options);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      $block = NULL;
      if ($entity = $this->entityRepository->loadEntityByConfigTarget('block_content', $this->options['target'])) {
        $block = $entity;
      }

      if (isset($block) && (!empty($this->options['bypass_access']) || $block->access('view'))) {
        $content = $this->entityTypeManager->getViewBuilder('block_content')
          ->view($block, $this->options['view_mode']);

        $build = [
          '#theme' => 'block',
          '#attributes' => [],
          '#configuration' => [
            'provider' => 'block-content--' . $entity->bundle(),
            'label' => $block->label(),
            'label_display' => $this->options['display_title'],
          ],
          '#base_plugin_id' => 'block_content',
          '#plugin_id' => 'block_content:' . $block->uuid(),
          '#derivative_plugin_id' => $block->uuid(),
          '#id' => $block->id(),
          'content' => $content,
          // Move contextual links up a level.
          '#contextual_links' => $content['#contextual_links'],
        ];
        return $build;
      }
    }

    return [];
  }

}
