INTRODUCTION
------------

Views can render Custom Blocks in an area out-of-the-box (using the Rendered
Entity area plugin), but because of the way that blocks are renderered, only the
block content is rendered, without the wrapper that the default
`block.html.twig` theme template provides.

This means that contextual links are not available, making block content
difficult for editors to access. Block labels are also not rendered.

This module provides a custom area plugin that renders a custom block using the
template that the block plugin uses.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Create a custom block, taking note of ID
 * Add an area to the header or footer of your view
 * Choose "Custom block"
 * Add the ID of the block, and any other required settings
