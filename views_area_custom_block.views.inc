<?php

/**
 * Implements hook_views_data().
 */
function views_area_custom_block_views_data() {
  $data['views']['views_area_custom_block'] = [
    'title' => t('Custom block'),
    'help' => t('Renders a custom block using the block template.'),
    'area' => [
      'id' => 'views_area_custom_block',
    ],
  ];

  return $data;
}
